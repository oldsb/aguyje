# -*- encoding: utf-8 -*-
import configparser
from os import path

CONFIG_FILE = "aguyje.conf"


class AguyjeConfiguration:
    def __init__(self):
        self.__configuration_parser = configparser.ConfigParser()
        absolute_file_path = path.join(path.abspath(path.dirname(__file__)), CONFIG_FILE)
        self.__configuration_parser.read(absolute_file_path)

    @property
    def neo4j_database_url(self):
        return self.__configuration_parser.get(section='neo4j', option='url')

    @property
    def neo4j_database_port(self):
        return self.__configuration_parser.get(section='neo4j', option='port')

    @property
    def neo4j_user(self):
        return self.__configuration_parser.get(section='neo4j', option='user')

    @property
    def neo4j_password(self):
        return self.__configuration_parser.get(section='neo4j', option='password')

    @property
    def log_file_name(self):
        return self.__configuration_parser.get(section='aguyje', option='log_file_name')

    @property
    def rabbit_url(self):
        return self.__configuration_parser.get(section='rabbit', option='url')
