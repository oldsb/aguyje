import logging
import threading
from datetime import datetime

import pytz
import rabbitpy
import requests as requests
from flask import Flask, jsonify, abort, request

from config_reader import AguyjeConfiguration
from ku2s import ku2s

EXCHANGE = 'threading_example'
QUEUE = 'deleteKudos'
ROUTING_KEY = 'test'
MESSAGE_COUNT = 100
app = Flask(__name__)
TIME_ZONE_BO = 'America/La_Paz'


@app.route("/")
def index():
    return "Welcome to KU2!"


def __get_datetime():
    datetime_bo = datetime.now(pytz.timezone(TIME_ZONE_BO))
    return datetime_bo.strftime('%d/%m/%Y %H:%M:%S')


@app.route('/aguyje/ku2s', methods=['GET'])
def get_ku2s():
    ku2_s = ku2s({}).get_all()
    datetime_str = __get_datetime()
    app.logger.info('[%s] [Get all ku2] id: %s', datetime_str, [ku2["id"] for ku2 in ku2_s])
    return jsonify(ku2_s), 200


def __get_description_browse_ku2(ku2, ku2_id):
    return 'Browse ku2 with id:%s', ku2['id'] if ku2 else 'No record found with id:%s', ku2_id


@app.route('/aguyje/ku2s/<int:ku2_id>', methods=['GET'])
def browse_ku2(ku2_id):
    ku2 = ku2s({}).browse(ku2_id)
    datetime_str = __get_datetime()
    description = __get_description_browse_ku2(ku2, ku2_id)
    app.logger.info('[%s] [Browse ku2] %s', datetime_str, description)
    return (jsonify(ku2), 200) if ku2 else (jsonify({'result': False}), 200)


@app.route('/aguyje/ku2s', methods=['POST'])
def create_ku2():
    if not request.json or not 'subject' in request.json:
        abort(400)
    ku2 = ku2s(request.json)
    datetime_str = __get_datetime()
    if get_users(ku2.receiver, ku2.sender):
        ku2_id = ku2.create()
        ku2.sync('newku2')
        app.logger.info('[%s] [Created ku2 ] with id:%s', datetime_str, ku2_id)
        return jsonify({'ku2_id': ku2_id}), 200
    else:
        app.logger.error('[%s] [Created ku2 ] No record found users', datetime_str)
        return jsonify('No record found users'), 200


@app.route('/aguyje/ku2s/<int:ku2_id>', methods=['DELETE'])
def delete_ku2(ku2_id):
    ku2_dict = ku2s({}).browse(ku2_id)
    ku2 = ku2s(ku2_dict)
    ku2.sync('deletedku2')
    ku2s({}).delete(ku2_id)
    datetime_str = __get_datetime()
    app.logger.info('[%s] [Deleted ku2] with id:%s', datetime_str, ku2_id)
    return jsonify({'result': True}), 200


def on_delete_kudos(connection):
    with connection.channel() as channel:
        for message in rabbitpy.Queue(channel, QUEUE).consume():
            receiver_id = eval(message.body)["msg"]
            message.ack()
            ku2s.delete_by_receiver_id(receiver_id)
            datetime_str = __get_datetime()
            app.logger.info('[%s] [Deleted ku2s] Deleted kudos for user: %s', datetime_str, receiver_id)


def get_users(receiver_id, sender_id):
    result = requests.get('http://localhost:3001/getUsers/' + receiver_id + '/' + sender_id)
    return eval(result.text).get('Result', False)


if __name__ == '__main__':
    configuration = AguyjeConfiguration()
    logging.basicConfig(filename=configuration.log_file_name, level=logging.INFO)
    rabbit = rabbitpy.Connection(configuration.rabbit_url)
    exchange = rabbitpy.Exchange(rabbit.channel(), EXCHANGE)
    exchange.declare()
    queue = rabbitpy.Queue(rabbit.channel(), QUEUE)
    queue.declare()
    queue.bind(EXCHANGE, ROUTING_KEY)
    kwargs = {'connection': rabbit}
    consumer_thread = threading.Thread(target=on_delete_kudos, kwargs=kwargs)
    consumer_thread.start()
    app.run(debug=True)
