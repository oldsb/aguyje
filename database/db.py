# -*- encoding: utf-8 -*-
from neo4j.v1 import GraphDatabase, basic_auth

from config_reader import AguyjeConfiguration


class Database:
    @property
    def session(self):
        return self._driver.session()

    def close(self):
        self._driver.close()

    def __repr__(self):
        return 'DB(url=%s)' % self._url

    def __init__(self):
        configuration = AguyjeConfiguration()
        self._url = configuration.neo4j_database_url
        self._user = configuration.neo4j_user
        self._password = configuration.neo4j_password
        self._driver = GraphDatabase.driver(self._url, auth=basic_auth(self._user, self._password))
