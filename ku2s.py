import os

import pika

from database import Database


class ku2s:
    def __init__(self, ku2):
        self._subject = ku2.get('subject', False)
        self._date = ku2.get('date', False)
        self._location = ku2.get('location', False)
        self._receiver = ku2.get('receiver', False)
        self._sender = ku2.get('sender', False)
        self._message = ku2.get('message', False)

    def sync(self, queue_name):
        url = os.environ.get('CLOUDAMQP_URL',
                             'amqp://pconpuub:Y8IjBDtuqg2BfSnGIkRXAb0bYjEgMbax@lion.rmq.cloudamqp.com/pconpuub')
        params = pika.URLParameters(url)
        connection = pika.BlockingConnection(params)
        channel = connection.channel()
        channel.queue_declare(queue=queue_name)
        channel.basic_publish(exchange='',
                              routing_key=queue_name,
                              body=self.receiver)
        connection.close()

    def create(self):
        create_ku2_query = '''
        MERGE (sender:Person{id:$sender})
        MERGE (receiver:Person{id:$receiver})
        MERGE (sender)-[r:KUDOS{subject:$subject, date:$date, location:$location, message:$message}]->(receiver)
        RETURN ID(r)
        LIMIT $limit
        '''
        parameters = dict(sender=self.sender, receiver=self.receiver, subject=self.subject, date=self.date,
                          location=self.location, message=self.message, limit=1)
        db = Database()
        ku2_id = db.session.run(create_ku2_query, parameters=parameters).single().value()
        db.close()
        return ku2_id

    @staticmethod
    def delete(ku2_id):
        delete_ku2_query = '''
                MATCH ()-[k:KUDOS]-() WHERE ID(k) = $ku2_id DELETE k;
                '''
        parameters = dict(ku2_id=ku2_id)
        db = Database()
        db.session.run(delete_ku2_query, parameters=parameters)
        db.close()

    @staticmethod
    def delete_by_receiver_id(receiver_id):
        delete_by_receiver_query = '''
                MATCH ()-[k:KUDOS]->(p:Person{id: $receiver_id}) DELETE k;
                '''
        parameters = dict(receiver_id=receiver_id)
        db = Database()
        db.session.run(delete_by_receiver_query, parameters=parameters)
        db.close()

    @staticmethod
    def browse(ku2_id):
        browse_ku2_query = '''
                MATCH ()-[k:KUDOS]-(r:Person) WHERE ID(k) = $ku2_id RETURN {id: ID(k), date: k.date, location:k.location, 
                message: k.message, subject:k.subject, receiver:r.id} LIMIT $limit;
                '''
        db = Database()
        # ku2 = db.session.run(browse_ku2_query, parameters={"ku2_id": ku2_id, "limit": 1}).single().value() or False
        record = db.session.run(browse_ku2_query, parameters={"ku2_id": ku2_id, "limit": 1}).single() or False
        ku2 = record.value() if record else False
        db.close()
        return ku2

    @staticmethod
    def get_all():
        get_all_query = '''
        MATCH ()-[k:KUDOS]->() RETURN {id: ID(k), date: k.date, location:k.location, 
                message: k.message, subject:k.subject};
        '''
        db = Database()
        ku2s = db.session.run(get_all_query).value()
        db.close()
        return ku2s

    @property
    def subject(self):
        return self._subject

    @property
    def date(self):
        return self._date

    @property
    def location(self):
        return self._location

    @property
    def receiver(self):
        return self._receiver

    @property
    def sender(self):
        return self._sender

    @property
    def message(self):
        return self._message

    @subject.setter
    def subject(self, subject):
        self._subject = subject

    @date.setter
    def date(self, date):
        self._date = date

    @location.setter
    def location(self, location):
        self._location = location

    @receiver.setter
    def receiver(self, receiver):
        self._receiver = receiver

    @sender.setter
    def sender(self, sender):
        self._sender = sender

    @message.setter
    def message(self, message):
        self._message = message

    def __repr__(self):
        return "Ku2s" % self.subject
